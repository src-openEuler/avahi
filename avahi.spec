%{?!WITH_QT5:     %global WITH_QT5    0}
%{?!WITH_PYTHON:  %global WITH_PYTHON 0}

Name:             avahi
Version:          0.8
Release:          21
Summary:          Avahi is a local network service discovery
License:          LGPL-2.1-or-later AND LGPL-2.0-or-later AND BSD-2-Clause-Views AND MIT
URL:              https://avahi.org
Source0:          https://avahi.org/download/%{name}-%{version}.tar.gz

## upstream patches
Patch0:           0000-avahi-dnsconfd.service-Drop-Also-avahi-daemon.socket.patch
Patch1:           0001-man-add-missing-bshell.1-symlink.patch
Patch2:           0002-Ship-avahi-discover-1-bssh-1-and-bvnc-1-also-for-GTK.patch
Patch3:           0003-fix-requires-in-pc-file.patch
Patch4:           0004-fix-bytestring-decoding-for-proper-display.patch
Patch5:           0005-avahi_dns_packet_consume_uint32-fix-potential-undefi.patch

Patch6001:        backport-CVE-2021-3468.patch
Patch6002:        backport-CVE-2021-36217.patch
Patch6003:        backport-CVE-2023-1981.patch
Patch6004:        backport-CVE-2023-38470.patch
Patch6005:        backport-CVE-2023-38473.patch
Patch6006:        backport-CVE-2023-38472.patch
Patch6007:        backport-CVE-2023-38471.patch
Patch6008:        backport-CVE-2023-38469.patch

BuildRequires:    gcc automake libtool desktop-file-utils gtk2-devel glib2-devel gcc-c++
BuildRequires:    libcap-devel expat-devel gdbm-devel make
BuildRequires:    intltool perl-XML-Parser systemd libevent-devel
BuildRequires:    dbus-devel >= 0.90 libdaemon-devel >= 0.11
BuildRequires:    pkgconfig(libevent) >= 2.0.21
%if 0%{?build_cross} == 0
BuildRequires:    pkgconfig(pygobject-3.0) xmltoman gtk3-devel >= 2.99.0
%endif

%if %{WITH_QT5}
BuildRequires:    qt5-qtbase-devel
%endif
%if %{WITH_PYTHON}
BuildRequires:    python2-dbus python2-libxml2
BuildRequires:    python2-devel
BuildRequires:    python3-devel
%else
Obsoletes:        python2-avahi < %{version}-%{release}
Obsoletes:        python3-avahi < %{version}-%{release}
Obsoletes:        avahi-ui-tools < %{version}-%{release}
%endif

Requires:         dbus expat libdaemon >= 0.11 %{name}-libs = %{version}-%{release}
Requires(pre):    shadow-utils coreutils
Requires(post):   dbus systemd
Requires(preun):  systemd
Requires(postun): systemd

%description
Avahi is a system which facilitates service discovery on a local network
via the mDNS/DNS-SD protocol suite. This enables you to plug your laptop
or computer into a network and instantly be able to view other people who
you can chat with, find printers to print to or find files being shared.

%package tools
Summary:          CMD tools for mDNS browsing and publishing
Requires:         %{name} = %{version}-%{release}
Requires:         %{name}-libs = %{version}-%{release}

%description tools
Command line tools that use avahi to browse and publish mDNS services.

%package ui-tools
Summary:          ui-tools for mDNS
Requires:         %{name} = %{version}-%{release}
Requires:         %{name}-libs = %{version}-%{release} 
Requires:         %{name}-glib = %{version}-%{release}
Requires:         %{name}-ui-gtk3 = %{version}-%{release}
Requires:         tigervnc openssh-clients
Conflicts:        %{name}-tools < 0.8-21
%if %{WITH_PYTHON}
Requires:         gdbm
Requires:         pygtk2
Requires:         pygtk2-libglade
Requires:         python2-avahi = %{version}-%{release}
Requires:         python2-dbus
Requires:         python2-gobject-base
%endif
%description ui-tools
Avahi Graphical user interface tools for mDNS services.

%if 0%{?build_cross} == 0
%package ui
Summary:          Gtk uesr interface library for Avahi (Gtk2)
Requires:         %{name}-libs = %{version}-%{release}
Requires:         %{name}-glib = %{version}-%{release} 

%description ui
This package contains a Gtk 2.x widget for browsing services.
%endif

%package autoipd
Summary:          Link-local IPv4 address automatic configuration daemon (IPv4LL)
Requires(pre):    shadow-utils
Requires:         %{name}-libs%{?_isa} = %{version}-%{release}
 
%description autoipd
avahi-autoipd implements IPv4LL, "Dynamic Configuration of IPv4
Link-Local Addresses"  (IETF RFC3927), a protocol for automatic IP address
configuration from the link-local 169.254.0.0/16 range without the need for a
central server. It is primarily intended to be used in ad-hoc networks which
lack a DHCP server.

%package dnsconfd
Summary:          This is useful for configuring unicast DNS servers in a DHCP-like fashion with mDNS
Requires:         %{name} = %{version}-%{release}
Requires:         %{name}-libs = %{version}-%{release}

%description dnsconfd
This is useful for configuring unicast DNS servers in a DHCP-like fashion with mDNS.

%package compat-howl
Summary:          Libraries for compat-howl
Requires:         %{name}-libs = %{version}-%{release}
Obsoletes:        howl-libs < %{version}-%{release}
Provides:         howl-libs = %{version}-%{release}

%description compat-howl
Libraries for compat-howl.

%package compat-howl-devel
Summary:          Header files and libs for howl compatibility libraries
Requires:         %{name}-compat-howl = %{version}-%{release}
Requires:         %{name}-devel = %{version}-%{release}
Obsoletes:        howl-devel < %{version}-%{release}
Provides:         howl-devel = %{version}-%{release}

%description compat-howl-devel
Header files and libs for howl compatibility libraries

%package compat-libdns_sd
Summary:          Libraries for compat-libdns_sd
Requires:         %{name}-libs = %{version}-%{release}

%description compat-libdns_sd
Libraries for compat-libdns_sd.

%package compat-libdns_sd-devel
Summary:          Header files and libs for compat-libdns_sd-devel
Requires:         %{name}-compat-libdns_sd = %{version}-%{release}
Requires:         %{name}-devel = %{version}-%{release}

%description compat-libdns_sd-devel
Header files and libs for compat-libdns_sd-devel

%package devel
Summary:           Libraries and header files for avahi development
Requires:          %{name}-libs = %{version}-%{release}
Requires:          %{name} = %{version}-%{release}

%description devel
Header files for using the avahi libraries.

%if %{WITH_PYTHON}
%package -n python2-avahi
Summary:          Python2 Avahi api
Obsoletes:        python-avahi < 0.7
Provides:         python-avahi = %{version}-%{release}
Requires:         %{name} = %{version}-%{release}
Requires:         %{name}-libs = %{version}-%{release}

%description -n python2-avahi
Python2 Avahi api.

%package -n python3-avahi
Summary:          Python3 Avahi api
Requires:         %{name} = %{version}-%{release}
Requires:         %{name}-libs = %{version}-%{release}

%description -n python3-avahi
Python3 Avahi api.
%endif

%package glib
Summary:          Glib libraries for avahi
Requires:         %{name}-libs = %{version}-%{release}

%description glib
Libraries for easy use of avahi from glib applications.

%package glib-devel
Summary:          Libraries and header files for avahi glib development
Requires:         %{name}-devel%{?_isa} = %{version}-%{release}
Requires:         %{name}-glib%{?_isa} = %{version}-%{release}
 
%description glib-devel
The avahi-devel package contains the header files and libraries
necessary for developing programs using avahi with glib.

%package gobject
Summary:          GObject wrapper library for Avahi
Requires:         %{name}-libs = %{version}-%{release}
Requires:         %{name}-glib = %{version}-%{release}

%description gobject
This library contains a GObject wrapper for the Avahi API

%package gobject-devel
Summary:          Libraries and header files for Avahi GObject development
Requires:         %{name}-devel%{?_isa} = %{version}-%{release}
Requires:         %{name}-gobject%{?_isa} = %{version}-%{release}
 
%description gobject-devel
The avahi-gobject-devel package contains the header files and libraries
necessary for developing programs using avahi-gobject.

%if 0%{?build_cross} == 0
%package ui-gtk3
Summary:          Gtk user interface library for Avahi (Gtk+ 3 version)
Requires:         %{name}-libs = %{version}-%{release}
Requires:         %{name}-glib = %{version}-%{release}

%description ui-gtk3
This library contains a Gtk 3.x widget for browsing services.

%package ui-devel
Summary:          Libraries and header files for Avahi UI development
Requires:         %{name}-devel%{?_isa} = %{version}-%{release}
Requires:         %{name}-ui%{?_isa} = %{version}-%{release}
Requires:         %{name}-ui-gtk3%{?_isa} = %{version}-%{release}

%description ui-devel
The avahi-ui-devel package contains the header files and libraries
necessary for developing programs using avahi-ui.
%endif

%package libs
Summary:          Libraries for avahi run-time use

%description libs
The avahi-libs package contains the libraries needed
to run programs that use avahi.

%if %{WITH_QT5}
%package qt5
Summary:         Qt5 libraries for avahi
Requires:        %{name}-libs = %{version}-%{release}

%description qt5
Libraries for easy use of avahi from Qt5 applications.

%package qt5-devel
Summary:         Libraries and header files for avahi Qt5 development
Requires:        %{name}-devel = %{version}-%{release}
Requires:        %{name}-qt5 = %{version}-%{release}

%description qt5-devel
The avahi-qt5-devel package contains the header files and libraries
necessary for developing programs using avahi with Qt5.
%endif

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

rm -fv docs/INSTALL


%build
NOCONFIGURE=1 ./autogen.sh

%configure --disable-monodoc --with-avahi-user=avahi \
        --with-avahi-group=avahi --with-avahi-priv-access-group=avahi \
        --with-autoipd-user=avahi-autoipd  --with-autoipd-group=avahi-autoipd \
        --with-systemdsystemunitdir=%{_unitdir}  --enable-introspection=no \
        --enable-shared=yes --enable-static=no  --disable-silent-rules \
        --enable-compat-libdns_sd --enable-compat-howl --disable-qt3 \
	%if 0%{?build_cross} == 0
        --disable-qt4 --disable-mono --enable-gtk\
	%else
        --disable-qt4 --disable-mono --disable-gtk --disable-gtk3\
	%endif
        --with-distro=none\
%if ! %{WITH_PYTHON}
    --disable-python \
%endif
%if ! %{WITH_QT5}
    --disable-qt5 \
%endif
;

%make_build -k V=1 || make V=1

%install
%make_install
%delete_la

# remove example
rm -fv %{buildroot}%{_sysconfdir}/avahi/services/ssh.service
rm -fv %{buildroot}%{_sysconfdir}/avahi/services/sftp-ssh.service

ln -s avahi-compat-howl.pc  %{buildroot}/%{_libdir}/pkgconfig/howl.pc
ln -s avahi-compat-libdns_sd.pc %{buildroot}/%{_libdir}/pkgconfig/libdns_sd.pc
ln -s avahi-compat-libdns_sd/dns_sd.h %{buildroot}/%{_includedir}/

install -d $RPM_BUILD_ROOT%{_localstatedir}/run/avahi-daemon
install -d $RPM_BUILD_ROOT%{_localstatedir}/lib/avahi-autoipd

mkdir -p $RPM_BUILD_ROOT/etc/avahi/etc
touch $RPM_BUILD_ROOT/etc/avahi/etc/localtime

%if %{WITH_PYTHON}
# Add python3 support
install -d %{buildroot}%{python3_sitelib}/avahi
cp -r $RPM_BUILD_ROOT%{python2_sitelib}/avahi/* $RPM_BUILD_ROOT%{python3_sitelib}/avahi/
rm -f $RPM_BUILD_ROOT%{python3_sitelib}/avahi/*.py{c,o}
sed -i 's!/usr/bin/python2!/usr/bin/python3!' $RPM_BUILD_ROOT%{python3_sitelib}/avahi/ServiceTypeDatabase.py

# avoid empty GenericName keys from .desktop files
for i in $RPM_BUILD_ROOT%{_datadir}/applications/*.desktop ; do
if [ -n "$(grep '^GenericName=$' $i)" ]; then
  desktop-file-edit --copy-name-to-generic-name $i
fi
done
%endif

%find_lang %{name}

%check
%if %{WITH_PYTHON}
for i in $RPM_BUILD_ROOT%{_datadir}/applications/*.desktop ; do
desktop-file-validate $i
done
%endif

%pre
%define gid_uid 70
if ! getent group avahi > /dev/null ; then
    groupadd -f -g %{gid_uid} -r avahi
fi

if ! getent passwd avahi > /dev/null ; then
  if ! getent passwd %{gid_uid} > /dev/null ; then
    useradd -r -l -u %{gid_uid} -g avahi -d %{_localstatedir}/run/avahi-daemon -s /sbin/nologin -c "Avahi mDNS/DNS-SD Stack" avahi
  else
    useradd -r -l -g avahi -d %{_localstatedir}/run/avahi-daemon -s /sbin/nologin -c "Avahi mDNS/DNS-SD Stack" avahi
  fi
fi

%define autoipd_gid_pid 170
if ! getent group avahi-autoipd >/dev/null ; then
    groupadd -f -g %{autoipd_gid_pid} -r avahi-autoipd
fi
if ! getent passwd avahi-autoipd > /dev/null ; then
  if ! getent passwd %{autoipd_gid_pid} > /dev/null; then
    useradd -r -u %{autoipd_gid_pid} -l -g avahi-autoipd -d %{_localstatedir}/lib/avahi-autoipd -s /sbin/nologin -c "Avahi IPv4LL Stack" avahi-autoipd
  else
    useradd -r -l -g avahi-autoipd -d %{_localstatedir}/lib/avahi-autoipd -s /sbin/nologin -c "Avahi IPv4LL Stack" avahi-autoipd
  fi
fi

exit 0

%preun
%systemd_preun avahi-daemon.socket avahi-daemon.service

%post
/usr/bin/dbus-send --system --type=method_call --dest=org.freedesktop.DBus / org.freedesktop.DBus.ReloadConfig >/dev/null 2>&1 || :
if [ "$1" -eq 1 -a -s /etc/localtime ]; then
        /usr/bin/cp -cfp /etc/localtime /etc/avahi/etc/localtime >/dev/null 2>&1 || :
fi
%systemd_post avahi-daemon.socket avahi-daemon.service

if [ $1 -eq 1 ]; then
    systemctl disable avahi-daemon.service
fi

%postun
%systemd_postun_with_restart avahi-daemon.socket avahi-daemon.service

%post dnsconfd
%systemd_post avahi-dnsconfd.service

%preun dnsconfd
%systemd_preun avahi-dnsconfd.service

%postun dnsconfd
%systemd_postun_with_restart avahi-dnsconfd.service

%files -f %{name}.lang
%doc README
%license LICENSE
%dir %{_sysconfdir}/avahi
%dir %{_sysconfdir}/avahi/etc
%ghost %{_sysconfdir}/avahi/etc/localtime
%config(noreplace) %{_sysconfdir}/avahi/hosts
%dir %{_sysconfdir}/avahi/services
%ghost %attr(0755, avahi, avahi) %dir %{_localstatedir}/run/avahi-daemon
%config(noreplace) %{_sysconfdir}/avahi/avahi-daemon.conf
%config(noreplace) %{_sysconfdir}/dbus-1/system.d/avahi-dbus.conf
%{_sbindir}/avahi-daemon
%dir %{_datadir}/avahi
%{_datadir}/avahi/*.dtd
%dir %{_libdir}/avahi
%if %{WITH_PYTHON}
%{_libdir}/avahi/service-types.db
%endif
%{_unitdir}/avahi-daemon.service
%{_unitdir}/avahi-daemon.socket
%{_datadir}/dbus-1/interfaces/*.xml
%{_datadir}/dbus-1/system-services/org.freedesktop.Avahi.service
%{_libdir}/libavahi-core.so.*

%files autoipd
%{_sbindir}/avahi-autoipd
%config(noreplace) %{_sysconfdir}/avahi/avahi-autoipd.action
%attr(1770,avahi-autoipd,avahi-autoipd) %dir %{_localstatedir}/lib/avahi-autoipd/

%files libs
%doc README
%license LICENSE
%{_libdir}/libavahi-common.so.*
%{_libdir}/libavahi-client.so.*
%{_libdir}/libavahi-libevent.so.*

%files glib
%{_libdir}/libavahi-glib.so.*

%files glib-devel
%{_libdir}/libavahi-glib.so
%{_includedir}/avahi-glib
%{_libdir}/pkgconfig/avahi-glib.pc

%files gobject
%{_libdir}/libavahi-gobject.so.*

%files gobject-devel
%{_libdir}/libavahi-gobject.so
%{_includedir}/avahi-gobject
%{_libdir}/pkgconfig/avahi-gobject.pc

%if 0%{?build_cross} == 0
%files ui-gtk3
%{_libdir}/libavahi-ui-gtk3.so.*

%files ui-devel
%{_libdir}/libavahi-ui.so
%{_libdir}/libavahi-ui-gtk3.so
%{_includedir}/avahi-ui
%{_libdir}/pkgconfig/avahi-ui.pc
%{_libdir}/pkgconfig/avahi-ui-gtk3.pc
%endif

%files  devel
%{_libdir}/libavahi-common.so
%{_libdir}/libavahi-core.so
%{_libdir}/libavahi-client.so
%{_libdir}/libavahi-libevent.so
%{_includedir}/avahi-client
%{_includedir}/avahi-common
%{_includedir}/avahi-core
%{_includedir}/avahi-libevent
%{_libdir}/pkgconfig/avahi-core.pc
%{_libdir}/pkgconfig/avahi-client.pc
%{_libdir}/pkgconfig/avahi-libevent.pc

%if %{WITH_PYTHON}
%files -n python2-avahi
%{python2_sitelib}/avahi/

%files -n python3-avahi
%{python3_sitelib}/avahi/
%endif

%files dnsconfd
%{_sbindir}/avahi-dnsconfd
%{_unitdir}/avahi-dnsconfd.service
%config(noreplace) %{_sysconfdir}/avahi/avahi-dnsconfd.action

%files tools
%{_bindir}/avahi-browse
%{_bindir}/avahi-browse-domains
%{_bindir}/avahi-publish
%{_bindir}/avahi-publish-address
%{_bindir}/avahi-publish-service
%{_bindir}/avahi-resolve
%{_bindir}/avahi-resolve-address
%{_bindir}/avahi-resolve-host-name
%{_bindir}/avahi-set-host-name

%files ui-tools
%{_bindir}/avahi-discover-standalone
%{_bindir}/bshell
%{_bindir}/bssh
%{_bindir}/bvnc
%{_datadir}/applications/b*.desktop
%{_datadir}/avahi/interfaces
%if %{WITH_PYTHON}
%{_bindir}/avahi-bookmarks
%{_datadir}/applications/avahi-discover.desktop
%{python2_sitelib}/avahi_discover/
%endif

%if 0%{?build_cross} == 0
%files ui
%{_libdir}/libavahi-ui.so.*
%endif

%files compat-howl
%{_libdir}/libhowl.so.*

%files compat-howl-devel
%{_libdir}/libhowl.so
%{_includedir}/avahi-compat-howl
%{_libdir}/pkgconfig/avahi-compat-howl.pc
%{_libdir}/pkgconfig/howl.pc


%files compat-libdns_sd
%{_libdir}/libdns_sd.so.*

%files compat-libdns_sd-devel
%{_libdir}/libdns_sd.so
%{_includedir}/avahi-compat-libdns_sd
%{_includedir}/dns_sd.h
%{_libdir}/pkgconfig/avahi-compat-libdns_sd.pc
%{_libdir}/pkgconfig/libdns_sd.pc

%if %{WITH_QT5}

%files qt5
%{_libdir}/libavahi-qt5.so.*

%files qt5-devel
%{_libdir}/libavahi-qt5.so
%{_includedir}/avahi-qt5/
%{_libdir}/pkgconfig/avahi-qt5.pc
%endif

%files help
%doc docs/* avahi-daemon/example.service avahi-daemon/sftp-ssh.service avahi-daemon/ssh.service
%{_mandir}/man?/*

%changelog
* Sun Aug 11 2024 Funda Wang <fundawang@yeah.net> - 0.8-21
- fix wrongly splitted tools and ui-tools
- cleanup spec

* Mon Jun 24 2024 zhangpan <zhangpan103@h-partners.com> - 0.8-20
- disable avahi-daemon.service default

* Sun Feb 4 2024 zhangpan <zhangpan103@h-partners.com> - 0.8-19
- delete redundant patch

* Mon Nov 6 2023 zhangpan <zhangpan103@h-partners.com> - 0.8-18
- fix CVE-2023-38469 CVE-2023-38471 CVE-2023-38472 CVE-2023-38473

* Mon Oct 16 2023 zhangpan <zhangpan103@h-partners.com> - 0.8-17
- fix CVE-2023-38470

* Wed Apr 12 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 0.8-16
- fix CVE-2023-1981

* Mon Mar 27 2023 zhangpan <zhangpan103@h-partners.com> - 0.8-15
- add build_cross to avoid install packages and files in self-build

* Tue Dec 20 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 0.8-14
- add BuildRequires make

* Sat Jun 11 2022 hanhui <hanhui15@h-partners.com> - 0.8-13
- DESC:avahi.rpm no longer providers avahi-autoiod.rpm

* Mon Nov 8 2021 hanhui <hanhui15@huawei.com> - 0.8-12
- Add avahi-glib-devel subpackages

* Mon Aug 30 2021 wangkerong <wangkerong@huawei.com> - 0.8-11
- Add autoipd,gobject-devel subpackages

* Wed Jul 14 2021 liuyuemng <liuyumeng5@huawei.com> - 0.8-10
- Type:CVE
- ID:CVE-2021-36217
- SUG:NA
- DESC:fix CVE-2021-36217

* Mon Jul 12 2021 zhanzhimin <zhanzhimin@huawei.com> - 0.8-9
- fix build failure due to lack of gcc-c++ buildrequires

* Tue Jun 22 2021 Wenlong Ding <wenlong.ding@turbolinux.com.cn> - 0.8-8
- Fix error spell of avahi-gobject.

* Mon Jun 21 2021 Wenlong Ding <wenlong.ding@turbolinux.com.cn> - 0.8-7
- Add requires avahi-gobeject when install avahi-devel.

* Tue Jun 15 2021 wangkerong <wangkerong@huawei.com> - 0.8-6
- Type:CVE
- ID:CVE-2021-3468
- SUG:NA
- DESC:fix CVE-2021-3468

* Thu May 20 2021 hanhui <hanhui15@huawei.com> - 0.8-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:avahi-devel buildrequire avahi-glib

* Thu May 13 2021 hanhui <hanhui15@huawei.com> - 0.8-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Split ui-devel packages from the devel package

* Thu Oct 29 2020 jinzhimin <jinzhimin2@huawei.com> - 0.8-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:remove pygtk2 in BuildRequires

* Fri Aug 7 2020 jinzhimin <jinzhimin2@huawei.com> - 0.8-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add provide glib2-devel to solve modify unresolvable err

* Fri Jul 31 2020 zhangqiumiao <zhangqiumiao1@huawei.com> - 0.8-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade to version 0.8

* Sat Feb 29 2020 hexiujun <hexiujun1@huawei.com> - 0.7-21
- Type:NA
- ID:NA
- SUG:NA
- DESC:use python2 explicitly in build environment

* Tue Feb 25 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.7-20
- Type:NA
- ID:NA
- SUG:NA
- DESC:modify fuzz test err

* Tue Dec 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.7-19
- rename docs subpackage as help subpackage

* Wed Oct 30 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.7-18
- Type:NA
- ID:NA
- SUG:NA
- DESC:Solve the problem of compilation failures

* Mon Sep 23 2019 hufeng <solar.hu@huawei.com> - 0.7-17
- Create spec
